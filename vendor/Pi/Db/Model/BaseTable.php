<?php
namespace Pi\Db\Model;

use Zend\ServiceManager\ServiceManager;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\HydratingResultSet;

class BaseTable {
	protected $tableGateway;

    public function __construct(TableGateway $tableGateway = null) {
        if($tableGateway){
            $this->tableGateway = $tableGateway;
        }
	}

	public function getLastInsertId() {
		return $this -> tableGateway -> lastInsertValue;
	}

	public function exchangeArray($data) {
		foreach ($data as $key => $value) {
			$this -> $key = (isset($data[$key])) ? $value : null;
		}
	}
    
    protected function getConfig() {
         return include __DIR__ . '/../../../../config/autoload/global.php';
    }

}
