<?php
namespace Pi\Helper;
use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceManager;

class BaseWidget extends AbstractHelper{

    protected $serviceLocator;
    
    public function __invoke(){
        return $this;
    }
    
    public function getTable($tableName){
        return $this->serviceLocator->get($tableName);
    }

    public function setServiceLocator(ServiceManager $serviceLocator){
        $this->serviceLocator = $serviceLocator;
    }
}

?>