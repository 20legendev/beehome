<?php 
namespace DVGroup\Common;
class Utf8
{
    //@tienhvt cat chuoi unicode
    public static function substr_unicode($str,$s=0,$l)
    {
        return join("", array_slice(preg_split("//u", $str, -1, PREG_SPLIT_NO_EMPTY), $s, $l));
      
        
    }
    //@tienhvt cat chuoi unicode tai vi spacebar gan nhat duoc chon
    public static function substr_unicode_space($str,$s=0,$l)
    {
        $arr_str=preg_split("//u", $str, -1, PREG_SPLIT_NO_EMPTY);
        while(isset($arr_str[$l])&&$arr_str[$l]!=' ')
        {
            $l+=1;
        }
        return join("",array_splice($arr_str,$s,$l));  
    }
}