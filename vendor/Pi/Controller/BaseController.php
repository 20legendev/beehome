<?php
namespace Pi\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Utf8;

class BaseController extends AbstractActionController {
		
	public function __construct(){
		$this->tables = array();
	}
			
	protected function getTable($table_name){
		if(isset($this->tables[$table_name])){
			return $this->tables[$table_name];
		}
		$this->tables[$table_name] = $this->getServiceLocator()->get($table_name);
		return $this->tables[$table_name];
 	}
 	
    protected function getAdapter() {
        return $this->getServiceLocator()->get('Adapter');
    }
	
	protected function setPaginator($cur_page, $total_page, $view){
	}
        
}
?>