<?php
namespace Album;
 use Zend\Db\ResultSet\ResultSet;
 use Zend\Db\TableGateway\TableGateway;

 use Album\Model\TbPlaylist;
 use Zend\Session\Container;
 use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use User\Model\Validator;

 class Module 
 {

    public function getAutoloaderConfig()
    {
         return array(
             'Zend\Loader\StandardAutoloader' => array(
                 'namespaces' => array(
                     __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                 ),
             ),
         );
     }

     public function getConfig()
     { 
         return include __DIR__ . '/config/module.config.php';
     }
     public function getServiceConfig()
     {
        return array(
            'factories'=>array(
                 'Album\Model\TbPlaylist' =>  function($sm) {
                     $tableGateway = $sm->get('TbPlaylistGateway');
                     $table = new TbPlaylist($tableGateway);					
                     return $table;
                 },
                 'TbPlaylistGateway' => function ($sm) {
                     $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                     //$resultSetPrototype = new ResultSet();
                     return new TableGateway('tb_artist_album_playlist', $dbAdapter, null,null);
                 },
            )
        );
     }
 }