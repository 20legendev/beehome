<?php

    return array(
        'controllers'        => array(
            'invokables' => array(
                'Subject\Controller\Index'       => 'Subject\Controller\IndexController',
                'Subject\Controller\SubjectHot'  => 'Subject\Controller\SubjectHotController',
                'Subject\Controller\Common'      => 'Subject\Controller\CommonController',
                'Subject\Controller\Widget'      => 'Subject\Controller\WidgetController',
                'Subject\Controller\SubjectUser' => 'Subject\Controller\SubjectUserController',
                'Subject\Controller\Detail'      => 'Subject\Controller\DetailController',
                'Subject\Controller\Playlist'    => 'Subject\Controller\PlaylistController',
                'Subject\Controller\Song'        => 'Subject\Controller\SongController',
            )
        ),
        'router'             => array(
            'routes' => array(
                'subject'     => array(
                    'type'    => 'Segment',
                    'options' => array(
                        'route'       => '/chu-de[.html]',
                        'constraints' => array(
                            //'controller' => '[a-zA-Z][a-zA-Z0-9-]*',
                            //'action'     => '[a-zA-Z][a-zA-Z0-9-]*',
                            // 'page' => '[1-9][0-9]*'
                        ),
                        'defaults'    => array(
                            '__NAMESPACE__' => 'Subject\Controller',
                            'controller'    => 'Subject\Controller\Index',
                            'action'        => 'index',
                            //'page' => '1'
                        )
                    )
                ),
                'subdetail'      => array(
                    'type'    => 'Segment',
                    'options' => array(
                        'route'       => '/chu-de[/:slug][/page=:page]',
                        'constraints' => array(
                            'controller' => '[a-zA-Z][a-zA-Z0-9-]*',
                            'action'     => '[a-zA-Z][a-zA-Z0-9-]*',
                            'slug'       => '[a-zA-Z][a-zA-Z0-9-]*',
                            'page'       => '[1-9][0-9]*'
                        ),
                        'defaults'    => array(
                            '__NAMESPACE__' => 'Subject\Controller',
                            'controller'    => 'detail',
                            'action'        => 'index',
                            'page'          => '1'
                        )
                    )
                ),
                'moredetail'      => array(
                    'type'    => 'Segment',
                    'options' => array(
                        'route'       => '[/:slug]/xem-tiep[/page=:page]',
                        'constraints' => array(
                            'slug'       => '[a-zA-Z][a-zA-Z0-9-]*',
                            'page'       => '[1-9][0-9]*'
                        ),
                        'defaults'    => array(
                            '__NAMESPACE__' => 'Subject\Controller',
                            'controller' => 'detail',
                            'action'     => 'viewmore',
                            'page'          => 1
                        )
                    ),

                ),
                'sbjplaylist' => array(
                    'type'    => 'Segment',
                    'options' => array(
                        'route'       => '/playlist[/:slug]',
                        'constraints' => array(
                            'controller' => '[a-zA-Z][a-zA-Z0-9-]*',
                            'action'     => '[a-zA-Z][a-zA-Z0-9-]*',
                            'slug'       => '[a-zA-Z][a-zA-Z0-9-]*',
                            'page'       => '[1-9][0-9]*'
                        ),
                        'defaults'    => array(
                            '__NAMESPACE__' => 'Subject\Controller',
                            'controller'    => 'playlist',
                            'action'        => 'index',
                            'page'          => '1'
                        )
                    )
                ),
                'song'        => array(
                    'type'    => 'Segment',
                    'options' => array(
                        'route'       => '/bai-hat[/:slug]',
                        'constraints' => array(
                            'controller' => '[a-zA-Z][a-zA-Z0-9-]*',
                            'action'     => '[a-zA-Z][a-zA-Z0-9-]*',
                            'slug'       => '[a-zA-Z][a-zA-Z0-9-]*',
                            'page'       => '[1-9][0-9]*'
                        ),
                        'defaults'    => array(
                            '__NAMESPACE__' => 'Subject\Controller',
                            'controller'    => 'song',
                            'action'        => 'index',
                            'page'          => '1'
                        )
                    )
                ),
                'subjectHot'  => array(
                    'type'    => 'Segment',
                    'options' => array(
                        'route'       => '/chu-de-hot[/page=:page]',
                        'constraints' => array(
                            'controller' => '[a-zA-Z][a-zA-Z0-9-]*',
                            'action'     => '[a-zA-Z][a-zA-Z0-9-]*',
                            'page'       => '[1-9][0-9]*'
                        ),
                        'defaults'    => array(
                            '__NAMESPACE__' => 'Subject\Controller',
                            'controller'    => 'subject-hot',
                            'action'        => 'index',
                            'page'          => '1',
                        )
                    )
                ),
                'subjectUser' => array(
                    'type'    => 'Segment',
                    'options' => array(
                        'route'       => '/chu-de-cua-ban[/page=:page]',
                        'constraints' => array(
                            'controller' => '[a-zA-Z][a-zA-Z0-9-]*',
                            'action'     => '[a-zA-Z][a-zA-Z0-9-]*',
                            'page'       => '[1-9][0-9]*'
                        ),
                        'defaults'    => array(
                            '__NAMESPACE__' => 'Subject\Controller',
                            'controller'    => 'subject-user',
                            'action'        => 'index',
                            'page'          => '1',
                        )
                    )
                ),


            ),
        ),
        'view_manager'       => array(
            'template_map'             => array(
                'layout/boxsearch'      => __DIR__ . '/../view/layout/box-search.phtml',
            ),
            'template_path_stack' => array(
                'Detail'  => __DIR__ . '/../view',
                'Subject' => __DIR__ . '/../view',
            ),
            'strategies'          => array(
                'ViewJsonStrategy'
            ),
        ),

        'controller_plugins' => array(
            'invokables' => array()
        ),
        'view_helpers'       => array(
            'invokables' => array(),
        ),
    );
