<?php

    namespace Subject\Controller;

    use DVGroup\Common\Params;
    use DVGroup\Redis\Redis;
    use DVGroup\Common\CommonLibs;
    use User\Model\Validator;
    use Zend\Mvc\Controller\AbstractActionController;
    use Zend\View\Model\ViewModel;
    use Zend\Predis\Client;
    use Zend\Paginator\Paginator,
        Zend\Paginator\Adapter\Null as PageNull;
    use Zend\Session\SessionManager;
    use Zend\Session\Container;

    class DetailController extends AbstractActionController
    {
        const TIMEOUT = 300; //5'
        const EXPIRE_CACHE = 7200; //2h
        const PER_PAGE = 8; // số bản ghi trên 1 trang, chủ đề hot
        const PAGE_RANGE = 5; //số phân trang
        public $CACHE = '_CACHE';
        public $unique = FALSE;
        public $SUBJECT = 'Subject';
        protected $redis;

        public function getRedis()
        {
            if (!$this->redis)
                $this->redis = new Redis();
            return $this->redis;
        }

        public function indexAction()
        {
            $view       = new ViewModel;
            $subSlug    = $this->params()->fromRoute('slug');
            $redis      = $this->getRedis();
            $subTable   = $this->getServiceLocator()->get('SubjectTable');
            $arrMusic   = array();
            $detailInfo = $subTable->getSubjectBySlug($subSlug);
            $page       = $this->params()->fromQuery('page', 1);

            //check subject type
            $subjectType = null;
            $template = null;
            if($detailInfo->hot>0){
                $subjectType = 'hot';
                $template = 'subject-hot';
            }
            if($detailInfo->created_by>0){
                $subjectType = 'user';
                $template = 'subject-user-list';
            }
            //set user visist
            $user_session = new Container('user');
            if (!$user_session->offsetExists($subSlug)) {
                $user_session->offsetSet($subSlug, session_id());
                $user_session->setExpirationSeconds(self::TIMEOUT);
                $this->unique = TRUE;
            }

            if ($this->unique) { //check user listen
                $subjectCountListen = $this->CACHE . ':SUBJECT:USER-ONLINE:' . $subSlug;
                $subjectUserlisten  = $this->SUBJECT . ':UserOnline:' . $subSlug;
                //insert cache
                $redis->incr($subjectCountListen);
                $redis->expire($subjectCountListen, self::TIMEOUT);
                //insert subject users listen
                $userlogin = new Validator();
                if ($userlogin->IsValid()) {
                    $username = $userlogin->getUser();
                    $redis->set($subjectUserlisten.':'.$username,$username,self::TIMEOUT);
                }else{
                    $redis->set($subjectUserlisten.':'.session_id(),session_id(),self::TIMEOUT);
                }
            }
            if (!is_null($detailInfo)) {
                $arrMusic                      = $this->getMusicBySucject($detailInfo->id, $subSlug, $page);
                $listMusic                     = $this->getScoreMusic($arrMusic['music']['arrMusic']);
                $arrMusic['music']['arrMusic'] = $listMusic;
            }

            $view->setVariables($arrMusic['music']);
            $view->setVariables(array(
                'detailInfo' => $detailInfo,
            ));
            $subjectRelated = $this->forward()->dispatch('Subject\Controller\Widget', array('action' => 'subject-process', 'page' => 1, 'perPage' => self::PER_PAGE, 'pageRange' => self::PAGE_RANGE, 'type' => $subjectType,'template'=>$template));
            $view->addChild($subjectRelated, 'subjectRelated');


            return $view;
        }

        public function viewmoreAction(){
            $view       = new ViewModel;
            $subSlug    = $this->params()->fromRoute('slug');
            $redis      = $this->getRedis();
            $subTable   = $this->getServiceLocator()->get('SubjectTable');
            $arrMusic   = array();
            $detailInfo = $subTable->getSubjectBySlug($subSlug);
            $page       = (int)$this->params()->fromQuery('page', 1);
            if($page<=0) $page =1;

            //check subject type
            $subjectType = null;
            $template = null;
            if($detailInfo->hot>0){
                $subjectType = 'hot';
                $template = 'subject-hot';
            }
            if($detailInfo->created_by>0){
                $subjectType = 'user';
                $template = 'subject-user-list';
            }
            if (!is_null($detailInfo)) {
                $arrMusic                      = $this->getMusicBySucject($detailInfo->id, $subSlug, $page);
                $listMusic                     = $this->getScoreMusic($arrMusic['music']['arrMusic']);
                $arrMusic['music']['arrMusic'] = $listMusic;
            }
            $view->setVariables($arrMusic['music']);
            $view->setVariables(array(
                'detailInfo' => $detailInfo,
            ));

            $subjectRelated = $this->forward()->dispatch('Subject\Controller\Widget', array('action' => 'subject-process', 'page' => 1, 'perPage' => self::PER_PAGE, 'pageRange' => self::PAGE_RANGE, 'type' => $subjectType,'template'=>$template));
            $view->addChild($subjectRelated, 'subjectRelated');
            return $view;
        }

        public function getMusicBySucject($subjectId, $slug, $page)
        { //$detailInfo->id
            $redis         = $this->getRedis();
            $subMusic      = $this->getServiceLocator()->get('SubjectMusicTable');
            $KeySubRd      = $this->CACHE . ':SUBJECT:MUSIC:' . $slug . ':' . 'PAGE:' . $page . ':LIMIT:' . self::PER_PAGE;
            $KeySubTotalRd = $this->CACHE . ':SUBJECT:MUSIC:' . $slug . ':' . 'TOTAL';

            if (!$redis->exists($KeySubTotalRd)) {
                $allRecord = $subMusic->getCountMusicBySubjectId($subjectId);
                $redis->set($KeySubTotalRd, $allRecord, $expire = self::EXPIRE_CACHE); //set redis
            } else {
                $allRecord = $redis->get($KeySubTotalRd);
            }

            $pageNull  = new PageNull($allRecord);
            $offset    = ($page - 1) * self::PER_PAGE;
            $paginator = new Paginator($pageNull);
            $paginator->setCurrentPageNumber($page);
            $paginator->setItemCountPerPage(self::PER_PAGE);
            $paginator->setPageRange(self::PAGE_RANGE);

            if (!$redis->exists($KeySubRd)) { //check exist redis
                $arrMusic = $subMusic->getMusicBySubject($subjectId, $offset, self::PER_PAGE);
                $redis->set($KeySubRd, CommonLibs::Zip($arrMusic), $expire = self::EXPIRE_CACHE);
            } else {
                $Object   = $redis->get($KeySubRd);
                $arrMusic = CommonLibs::UnZip($Object);
            }
            $result = [
                'music' => [
                    'arrMusic'     => $arrMusic,
                    'paginator'    => $paginator,
                    'allRecord'    => $allRecord,
                    'offset'       => $offset,
                    'itemsPerPage' => self::PER_PAGE,
                ]
            ];
            return $result;
        }

        public function getScoreMusic($arrMusic = null)
        {
            if (!is_null($arrMusic)) {
                foreach ($arrMusic as $k => $music) {
                    $score_common = CommonLibs::getAvgScoreMusic($music['identify']); //diểm cộng đồng
                    $userlogin                     = new Validator();
                    $score_friends                 = ($userlogin->IsValid()) ? CommonLibs::getAvgScoreMusicByFriends($music['identify'], $userlogin->getUser()) : 0; //điểm bạn bè
                    $arrMusic[$k]['score_common']  = round($score_common, 2);
                    $arrMusic[$k]['score_friends'] = round($score_friends, 2);
                }
            }
            return $arrMusic;
        }


    }
