<?php

    namespace Subject\Controller;

    use Zend\Mvc\Controller\AbstractActionController;
    use Zend\View\Model\ViewModel;

    class PlaylistController extends AbstractActionController
    {

        public function IndexAction()
        {

            $view = new ViewModel();


            $slug = $this->params()->fromRoute('slug');
            echo $slug;
            return $view;
        }
    }