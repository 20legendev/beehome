<?php

    namespace Subject\Controller;

    use DVGroup\Redis\Redis;
    use Redis\Source\CommonLibs;
    use Zend\Json\Json;
    use Zend\Mvc\Controller\AbstractActionController;
    use Zend\View\Model\ViewModel;
    use Zend\Paginator\Paginator,
        Zend\Paginator\Adapter\Null as PageNull;
    use Zend\ModuleManager;


    class WidgetController extends AbstractActionController
    {

        const EXPIRE_CACHE = 7200;
        public $CACHE = '_CACHE';
        public $QUEUE = 'queue_rd:a:';
        public $QUEUE_LAST_INSERT = 'queue_rd:s:id';
        public $SUBJECT = 'Subject';
        protected $redis;

        //lấy chủ đề theo type = hot,user,..
        public function subjectTypeAction($page = NULL, $perPage = NULL, $pageRange = NULL, $type = NULL, $subjectId = NULL)
        {

            $page       = $page != NULl ? $page : $this->params('page'); //trang hiện tại
            $subjectId  = $subjectId != NULl ? $subjectId : $this->params('subjectId'); //trang hiện tại
            $perPage    = $perPage != NULL ? $perPage : $this->params('perPage'); //số bản ghi trên 1 trang
            $pageRange  = $pageRange != NULL ? $pageRange : $this->params('pageRange'); //số trang hiển thị
            $type       = $type != NULL ? $type : $this->params('type'); //kiểu dữ liệu
            $subTable   = $this->getServiceLocator()->get('SubjectTable');
            $arrSubject = array();
            $pageNull   = null;
            $redis      = $this->getRedis();

            switch ($type) {
                case 'hot': //cache chủ đề hot
                    $KeySubRd      = $this->CACHE . ':SUBJECT:HOT:' . 'PAGE:' . $page . ':LIMIT:' . $perPage;
                    $KeySubTotalRd = $this->CACHE . ':SUBJECT:HOT:' . 'TOTAL';
                    $where         = array('hot>0');
                    $order         = 'hot DESC';
                    break;
                case 'user': //cache chu de nguoi dung
                    $KeySubRd      = $this->CACHE . ':SUBJECT:USER:' . 'PAGE:' . $page . ':LIMIT:' . $perPage;
                    $KeySubTotalRd = $this->CACHE . ':SUBJECT:USER:' . 'TOTAL';
                    $where         = array('created_by>0');
                    $order         = 'order ASC';
                    break;
            }
            $allRecord = 0;
            if (!$redis || ($redis && !$redis->exists($KeySubTotalRd))) {
                $allRecord = $subTable->countAll($where);
                if ($redis) $redis->set($KeySubTotalRd, $allRecord, self::EXPIRE_CACHE); //insert redis
            } else {
                $allRecord = $redis->get($KeySubTotalRd);
            }
            //paging
            $pageNull  = new PageNull($allRecord);
            $offset    = ($page - 1) * $perPage;
            $paginator = new Paginator($pageNull);
            $paginator->setCurrentPageNumber($page);
            $paginator->setItemCountPerPage($perPage);
            $paginator->setPageRange($pageRange);
            //get data
            $arrSubject = $subTable->getData($type, $KeySubRd, $where, $offset, $perPage);
            $result     = array(
                'subject' => array(
                    'data'         => $arrSubject,
                    'paginator'    => $paginator,
                    'allRecord'    => $allRecord,
                    'offset'       => $offset,
                    'itemsPerPage' => $perPage,
                )
            );
            return $result;
        }

        public function subjectProcessAction()
        {
            $page          = $this->params('page'); //trang hiện tại
            $perPage       = $this->params('perPage'); //số bản ghi trên 1 trang
            $pageRange     = $this->params('pageRange'); //số trang hiển thị
            $type          = $this->params('type'); //kiểu dữ liệu
            $template      = $this->params('template'); //kiểu dữ liệu
            $subject       = $this->subjectTypeAction($page, $perPage, $pageRange, $type);
            $view          = new ViewModel();
            $view->subject = $subject['subject']['data'];
            switch ($template) {
                case 'subject-hot':
                    $view->setTemplate('subject/widget/subject-hot.phtml');
                    break;
                case 'subject-user':
                    $view->setTemplate('subject/widget/subject-user.phtml');
                    break;
                case 'subject-user-list':
                    $view->setTemplate('subject/widget/subject-user-list.phtml');
                    break;
            }
            return $view;
        }

        public function getRedis()
        {
            if (!$this->redis) {
                $this->redis = new Redis();
            }
            return $this->redis;
        }

    }