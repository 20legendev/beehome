<?php

    namespace Subject\Controller;

    use Redis\Source\CommonLibs;
    use Zend\Mvc\Controller\AbstractActionController;
    use Zend\View\Model\ViewModel;
    use Zend\Predis\Client;
    use Zend\Paginator\Paginator,
        Zend\Paginator\Adapter\Null as PageNull;
    use Subject\Controller\IndexController;

    class SubjectUserController extends AbstractActionController
    {

        const PER_PAGE = 15; // số bản ghi trên 1 trang, chủ đề hot
        const PAGE_RANGE = 5; //số phân trang

         public function indexAction()
        {

            $view        = new ViewModel;
            $page       = (int)$this->params()->fromQuery('page', 1);
            if($page<=0) $page =1;
            $subjectUser = $this->forward()->dispatch('Subject\Controller\Widget', array('action' => 'subject-type', 'page' => $page, 'perPage' => self::PER_PAGE, 'pageRange' => self::PAGE_RANGE, 'type' => 'user'));
            $view->setVariables($subjectUser->subject);
            return $view;
        }
    }