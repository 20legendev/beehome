<?php
namespace User\Form;

use Zend\Form\Element;
use Zend\Form\Element\Text;
use Zend\Form\Element\Password;

use Zend\Form\Element\Checkbox;
use Zend\Form\Form;
use Zend\Form\Element\Csrf;

class LoginForm extends Form
{   

    public function __construct($name)
    {
        parent::__construct($name);
        $this->setAttribute('method', 'post');
        $this->addlelement();
    }
    public function addlelement()
    {        
        $username=new Text('username');
        $username->setLabel('Tên Đăng Nhập:*')
                 ->setAttribute('class','form-control')
                 ->setAttribute('required',true);
        $this->add($username);
        
        $password=new Password('password');
        $password->setLabel('Mật Khẩu:*')
                 ->setAttribute('class','form-control')
                 ->setAttribute('required',true);
        $this->add($password);


        $sdt=new Checkbox('remember');
        $sdt->setLabel('Remember:');
           // ->setAttribute('class','form-control');
            $this->add($sdt);
        $security=new Csrf('security');
        $this->add($security);
  
        
    }

}