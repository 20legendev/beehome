<?php
namespace User\Controller;

use DVGroup\Auth\AuthUser;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use User\Form\UserForm;
use User\Form\LoginForm;

class IndexController extends AbstractActionController
{
    protected function getAuthService()
    {
      return $this->getServiceLocator()->get('AuthService');
    }

  /**
   * xu ly dang ky tai khoan
   * @return ViewModel
   * @throws \Zend\Crypt\Password\Exception
   */
    public function registerAction()
    {
      $view    = new ViewModel();
      $form    = new UserForm('login');
      $request = $this->getRequest();
      // echo \Zend\Math\Rand::getString(20,Null,1);
      if ($request->isPost()) {
        $form->setData($request->getPost());
        if ($form->isValid()) {
          $data = $form->getData();
          if ($data['password'] == $data['repassword'] && preg_match('/^[a-zA-Z0-9_]+$/', $data['username'])) {
            $data['password'] = \Zend\Crypt\Password\Bcrypt::generatePasswordHash($data['password']);
            //var_dump($data);
            $view->check = $this->getTbUser()->register($data);
          } else {
            $view->check = 0;
            //var_dump($data);

          }

        }
      }
      $view->setVariable('login', $form);
      return $view;
    }

  /**
   * xu ly dang nhap
   * @return \Zend\Http\Response|ViewModel
   */
    public function loginAction()
    {

      if ((new AuthUser())->isAuthen()) {
        $this->redirect()->toRoute('home');
      }
      //session_start();
      //var_dump($_SESSION);die();
      $view    = new ViewModel();
      $form    = new LoginForm('login');
      $request = $this->getRequest();
      if ($request->isPost()) {
        $form->setData($request->getPost());
        if ($form->isValid()) {
          $data  = $form->getData();
          $check = $this->getTbUser()->login($data);
          if ($check == 1) return $this->redirect()->toRoute('home');
          else echo 'fail';
        }
      }
      $view->setVariable('login', $form);
      return $view;
    }

  /**
   * xu ly logout
   * @return \Zend\Http\Response
   */
    public function logoutAction()
    {
      $auth = new AuthUser();
      $auth->destroy();
      return $this->redirect()->toRoute('home');
    }

    //phan kiem tra dang nhap cho user nam o module;
  /**
   * xu ly trang ca nhan
   * @return \Zend\Http\Response|ViewModel
   */
    public function profileAction()
    {
      $view     = new ViewModel();
      $username = $this->params()->fromRoute('username', 0);
      $auth     = new AuthUser();
      if (!$auth->isUser($username)) {
        $username = $auth->getUsername();
        return $this->redirect()->toRoute('Profile', array('action' => 'profile', 'username' => $username));
      }
      $user = $this->getTbUser()->getUserByUsername($username);
      return $view;

    }

  /**
   * lien ket TbUser model
   * @return array|object
   */
    private function getTbUser()
    {
      return $this->getServiceLocator()->get('TbUser');
    }
}
