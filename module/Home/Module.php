<?php
    namespace Home;

    use Zend\Mvc\ModuleRouteListener;
    use Zend\Mvc\MvcEvent;

    class Module
    {

        public function onBootstrap(MvcEvent $e) //set layout auto
        {
            $eventManager = $e->getApplication()->getEventManager();
            $eventManager->getSharedManager()->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function ($e) {
                $controller      = $e->getTarget();
                $controllerClass = get_class($controller);
                $moduleNamespace = substr($controllerClass, 0, strpos($controllerClass, '\\'));
                $config          = $e->getApplication()->getServiceManager()->get('config');
                if (isset($config['module_layouts'][$moduleNamespace])) {
                    $controller->layout($config['module_layouts'][$moduleNamespace]);
                }
            }, 100);
            $moduleRouteListener = new ModuleRouteListener();
            $moduleRouteListener->attach($eventManager);
			$eventManager->attach(MvcEvent::EVENT_DISPATCH, array($this, 'manage'), 100);
        }
        public function manage(MvcEvent $e)
        {
            
        }

        public function getConfig()
        {
            $layout='default';
            define('LAYOUT_FOLDER','layout/'.$layout.'/');
            define('LAYOUT_DEFAULT',__DIR__.'/view/layout/'.$layout.'/layout');
            return include __DIR__ . '/config/module.config.php';
        }

        public function getAutoloaderConfig()
        {
            return array(
                'Zend\Loader\StandardAutoloader' => array(
                    'namespaces' => array(
                        __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                        'Pi' => __DIR__ .'/../../vendor/Pi',
                    ),
                ),
            );
        }

		public function getViewHelperConfig() {
			return [
				'factories' => [
					'getMenuManager' => function ($sm) {
						$viewHelper = new \Category\Widget\MenuManager();
						$viewHelper->setServiceLocator($sm->getServiceLocator());
						return $viewHelper;
					}
				]
			];
		}
	};
?>