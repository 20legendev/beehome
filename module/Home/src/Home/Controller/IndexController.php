<?php

    namespace Home\Controller;

    use Zend\Mvc\Controller\AbstractActionController;
    use Zend\View\Model\ViewModel;
	use Pi\Controller\BaseController;

    class IndexController extends BaseController
    {

        public function indexAction(){
            $view = new ViewModel();
            return $view;
        }

    }
