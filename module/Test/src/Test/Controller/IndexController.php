<?php
namespace Test\Controller;
use DVCommon\LibRedis\ConnectRedis;
use Redis\Source\Redis;
use Zend\Authentication\Storage\Session;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Config\SessionConfig;
use Zend\Session\Config\StandardConfig;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use Zend\Cache\StorageFactory;
use Zend\Session\SaveHandler\Cache;
use Zend\Session\SessionManager;
use Zend\Predis\Client;
use Redis\Service\RedisService;
use DVGroup\Neo4j\Neo4j;
use Everyman\Neo4j\Cypher\Query;

 class IndexController extends AbstractActionController
 {
     private static  $redis;

    public function autocacheAction(){
        $tableTest = $this->getServiceLocator()->get('TableTest');
        $tableTest->test();
        return $this->response;
        
        
    }
    public function neoAction()
    {
        $neo=new Neo4j();
        var_dump($neo);
    }

     public function getRedis()
     {
         try {
             $config = $this->getServiceLocator()->get('connects');
             self::$redis = new Client($config);
         }
         catch (Exception $e) {
             echo "Couldn't connected to Redis";
             echo $e->getMessage();
         }
         return self::$redis;
     }

     public function indexAction()
     {

         $subSlug = 'con-mua-tinh-yeu';
         $redis = $this->getRedis();
         $unique = FALSE;

         $user_session = new Container('user');

         if(!$user_session->offsetExists('user')){
             $user_session->offsetSet('user', 1);
             $user_session->setExpirationSeconds('60');
             $unique = TRUE;
         }

        //set time variables
         $day = date('Y-m-d');

         if ($unique) {
             $redis->incr($subSlug.":" . $day);
             $redis->expire($subSlug.":" . $day,60);
         }

         echo  $redis->get($subSlug.':' . $day);
die;

//         $redis->incr();
//         $redis->expire($subSlug,60);
//         $value = $redis->get($subSlug);
//         echo $value;die;
    }

    public function solrAction()
    {
            $options = array
        (
        'hostname' => SOLR_SERVER_HOSTNAME,
        //'login'    => SOLR_SERVER_USERNAME,
       // 'password' => SOLR_SERVER_PASSWORD,
        'port'     => SOLR_SERVER_PORT,
        );
        
        $client = new \SolrClient($options);
        
        $doc = new \SolrInputDocument();
        
//        $doc->addField('id', 334455);
//        $doc->addField('cat', 'Software');
//        $doc->addField('cat', 'Lucene');
        
      //  $client = new SolrClient($options);
        
        $query = new \SolrQuery();
        
        $query->setQuery('lucene');
        
        $query->setStart(0);
        
        $query->setRows(50);
        
        $query->addField('cat')->addField('features')->addField('id')->addField('timestamp');
        
        $query_response = $client->query($query);
        
        $response = $query_response->getResponse();
        
        var_dump($response);
    }
    public function jsonAction()
    {
        $this->layout('layout\home');
        $view=new \Zend\View\Model\JsonModel();
        $view->setVariables(['s1'=>'1','s2'=>'2']);
        return $view;
    }
    public function testpassAction()
    {
        $kk=$this->getTest();
      //  var_dump($kk->test());
       // $l=new \Test\Model\Test($this->getServiceLocator()->get('redis'));
        var_dump($l->test());
    }
    public function getTest()
    {
       var_dump($this->getServiceLocator());
        return $this->getServiceLocator()->get('Test');
    }
    
   




 }
?>