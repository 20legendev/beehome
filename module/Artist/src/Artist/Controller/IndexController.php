<?php

namespace Artist\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Artist\Model\TbArtist;
use Cate\Model\Cate;
use Utf8;
use Cate\Model\CateTable;
use Cate\Model\TbMusicTitleForead;
use Zend\Paginator\Paginator;

use DVGroup\Operation\BaseController;

class IndexController extends BaseController {

	public function indexAction(){
        $this->layout('layout/artist');
		$view = new ViewModel();
		$params = $this->params();
		$name = $params->fromRoute('name');

		$artist = $this->prepare($name, $view, "profile");

		$playlist_obj = $this->getTable('Album\Model\TbPlaylist');
		$playlist_data = $playlist_obj->getAlbumByArtist($artist['artist_id']);
		unset($playlist_obj);

		$music = $this->getTable('Cate\Model\TbMusicTitleForead');
		$music_data = $music->getFullByArtistId($artist['artist_id']);

		$view->artist = $artist;
		$view->music = $music_data;
       	$view->playlist = $playlist_data;
       	return $view;
    }

	public function songAction(){
		$view = new ViewModel();
		$params = $this->params();
		$name = $params->fromRoute('name');
		$page = $params->fromRoute('page', 1);
		$page = $page - 1;
		
		$artist = $this->prepare($name, $view, "song");
		
		$music = $this->getTable('Cate\Model\TbMusicTitleForead');
		$music_obj = $music->getFullByArtistId($artist['artist_id'], 0, 12, $page);
		$total = $music->countByArtistId($artist['artist_id'], 0, 12);
		$view->music = $music_obj;
		$view->artist = $artist;
		$view->is_next = $page + 1 < $total ? $page + 2 : -1;
		$view->is_prev = $page > 0 ? $page : -1;
		return $view;
	}
	
	public function albumAction(){
		$view = new ViewModel();
		$params = $this->params();
		$name = $params->fromRoute('name');
		$page = $params->fromRoute('page', 1);
		$page = $page - 1;
		
		$artist = $this->prepare($name, $view, "album");
		
		$playlist = $this->getTable('Album\Model\TbPlaylist');
		$album = $playlist->getAlbumByArtist($artist['artist_id'], 12, $page);
		$total = ceil($playlist->countByArtist($artist['artist_id']) / 12);
		
		$view->artist = $artist;
		$view->album = $album;
		$view->is_next = $page + 1 < $total ? $page + 2 : -1;
		$view->is_prev = $page > 0 ? $page : -1;
		return $view;
	}
	
	public function videoAction(){
		$view = new ViewModel();
		$params = $this->params();
		$name = $params->fromRoute('name');
		$page = $params->fromRoute('page', 1);
		$page = $page - 1;
		
		$artist = $this->prepare($name, $view, "video");
		
		$playlist = $this->getTable('Album\Model\TbPlaylist');
		$album = $playlist->getAlbumByArtist($artist['artist_id'], 12, $page);
		$total = ceil($playlist->countByArtist($artist['artist_id']) / 12);
		
		$view->artist = $artist;
		$view->album = $album;
		$view->is_next = $page + 1 < $total ? $page + 2 : -1;
		$view->is_prev = $page > 0 ? $page : -1;
		return $view;
	}
	
	public function newsAction(){
		$view = new ViewModel();
		$params = $this->params();
		$name = $params->fromRoute('name');
		$page = $params->fromRoute('page', 1);
		$page = $this->getRealPage($page);
	
		$artist = $this->prepare($name, $view, "news");
		$article_tag = $this->getTable('ArticleTag');
		$article = $article_tag->getNewsByTag($name, 12, $page);
		//return var_dump($article);
		$total = ceil($article_tag->countByTag($name) / 12);
		
		$view->artist = $artist;
		$view->article = $article;
		$view->name = $name;
		$view->is_next = $page + 1 < $total ? $page + 2 : -1;
		$view->is_prev = $page > 0 ? $page : -1;
		return $view;
	}
	
	private function prepare($name, &$view, $slug){
		$artist = $this->getProfile($name);
		$profile = $this->forward()->dispatch('Artist\Controller\Widget', array(
			'action'=>'top-profile',
			'slug'=>$slug,
			'name' => $name
		));
		$left_profile = $this->forward()->dispatch('Artist\Controller\Widget', array(
			'action'=>'left-profile',
			'name' => $name
		));
		$artist_related = $this->forward()->dispatch('Cate\Controller\Widget', array(
			'action' => 'artist-related',
			'artist_id'=>$artist['artist_id'],
			'rand'=> ((int) $artist['artist_id'])%5 + 5,
			'limit'=> 10
		));
		$view->addChild($left_profile, 'left_profile');
		$view->addChild($artist_related,'artist_related');
		$view->addChild($profile, 'profile');
		return $artist;
	}
	
	
	private function getProfile($name){
		$artist_content = $this->getTable('Artist\Model\ArtistContent');
		$artist_arr = $artist_content->getByNameSearch($name);
		if(!$artist_arr || empty($artist_arr)){
			return NULL;
		}else{
			return $artist_arr[0];
		}
	}
	
	private function getRealPage($page){
		return intval($page) - 1;
	}
}
