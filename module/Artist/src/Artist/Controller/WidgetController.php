<?php
namespace Artist\Controller;

use Zend\View\Model\ViewModel;
use DVGroup\Operation\BaseController;

class WidgetController extends BaseController {

	public function topProfileAction() {
		$view = new ViewModel();
		$params = $this->params();
		$name = $params->fromRoute('name');
		$slug = $params->fromRoute('slug', 'profile');
		$view->artist = $this->getProfile($name);
		$view->slug = $slug;
		return $view;
	}
	
	public function leftProfileAction() {
        $this->layout('layout/artist');
		$view = new ViewModel();
		$params = $this->params();
		$name = $params->fromRoute('name');
		$view->artist = $this->getProfile($name);
		return $view;
	}
	
	/**
	 * Artist widget inside the music play page
	 * @return \Zend\View\Model\ViewModel
	 */
	
	public function artistMusicAction() {
		$view = new ViewModel();
		$params = $this->params();
		$artist_id = $params->fromRoute('artist_id');
			
		$artist_content = $this->getTable('Artist\Model\ArtistContent');
		$artist_obj = $artist_content->getByArtistId($artist_id);
		
		$related = $this->forward ()->dispatch ( 'Cate\Controller\Widget', array (
				'action' => 'artist-related',
				'artist_id' => $artist_id
		) );
		
		$view->addChild($related, 'related');
		$view->artist = $artist_obj;
		return $view;
	}
	
	public function mediaRelatedAction() {
		$view = new ViewModel();
		$params = $this->params();
		$artist_id = $params->fromRoute('artist_id');
		$type = $params->fromRoute('type', 0);
		
		$artist = $this->getTable('Artist\Model\Artist');
		$obj = $artist->getById($artist_id);
		if(!$obj){
			return $this->layout('error/404');
		}
		$music_obj = $this->getTable('Cate\Model\TbMusicTitleForead');
		$music = $music_obj->getRelated($obj['cate_id'], $type);
		$view->music = $music;
		return $view;
	}
	
	private function getProfile($name){
		$artist_content = $this->getTable('Artist\Model\ArtistContent');
		$artist_arr = $artist_content->getByNameSearch($name);
		if(!$artist_arr || empty($artist_arr)){
			return NULL;
		}else{
			return $artist_arr[0];
		}
	}
	
	
	

}
