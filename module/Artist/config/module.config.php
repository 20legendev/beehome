<?php
return array (
		'controllers' => array (
				'invokables' => array (
						'Artist\Controller\Index' => 'Artist\Controller\IndexController',
						'Artist\Controller\Widget' => 'Artist\Controller\WidgetController' 
				) 
		),
		'router' => array (
				'routes' => array(
            	/* update to chung */
                'ArtistDetail' => array (
								'type' => 'Segment',
								'options' => array (
										'route' => '/nghe-si[/:name]',
										'constraints' => array (
												'name' => '[a-zA-Z0-9_-]*'
										),
										'defaults' => array (
												'controller' => 'Artist\Controller\Index',
												'action' => 'index' 
										) 
								),
								'may_terminate' => true,
								'child_routes' => array (
										'song' => array (
												'type' => 'segment',
												'options' => array (
														'route' => '/bai-hat[.trang-:page].html',
														'constraints' => array (
																'page' => '[0-9]*' 
														),
														'defaults' => array (
																'controller' => 'Artist\Controller\Index',
																'action' => 'song' 
														) 
												) 
										),
										'album' => array (
												'type' => 'segment',
												'options' => array (
														'route' => '/album[.trang-:page].html',
														'constraints' => array (
																'page' => '[0-9]*' 
														),
														'defaults' => array (
																'controller' => 'Artist\Controller\Index',
																'action' => 'album' 
														) 
												) 
										),
										'video' => array (
												'type' => 'segment',
												'options' => array (
														'route' => '/video-clip[.trang-:page].html',
														'constraints' => array (
																'page' => '[0-9]*' 
														),
														'defaults' => array (
																'controller' => 'Artist\Controller\Index',
																'action' => 'video' 
														) 
												) 
										),
										'news' => array (
												'type' => 'segment',
												'options' => array (
														'route' => '/tin-tuc[.trang-:page].html',
														'constraints' => array (
																'page' => '[0-9]*' 
														),
														'defaults' => array (
																'controller' => 'Artist\Controller\Index',
																'action' => 'news' 
														) 
												) 
										) 
								) 
						) 
				) 
		),
		'view_manager' => array (
				'display_not_found_reason' => true,
				'display_exceptions' => true,
				'doctype' => 'HTML5',
				'not_found_template' => 'error/404',
				'exception_template' => 'error/index',
				'template_map' => array (
						'layout/artist' => __DIR__ . '/../view/layout/layout.phtml' 
				),
				'template_path_stack' => array (
						'Artist' => __DIR__ . '/../view' 
				),
				'strategies' => array (
						'ViewJsonStrategy' 
				) 
		),
		
		'controller_plugins' => array (
				'invokables' => array () 
		),
		'view_helpers' => array (
				'invokables' => array () 
		) 
);
