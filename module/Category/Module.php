<?php
namespace Category;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Category\Model\Category;

use Zend\Session\Container;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module {
	public function onBootstrap(MvcEvent $e) {
		$eventManager = $e -> getApplication() -> getEventManager();
		$moduleRouteListener = new ModuleRouteListener();
		$moduleRouteListener -> attach($eventManager);
		// $eventManager -> attach(MvcEvent::EVENT_DISPATCH, array($this, 'preload'), 100);\
	}


	//@tienhvt ham nay load truoc khi thuc thi cac action trong controller,
	//cac action can phai login
	//set ngon ngu mac dinh

	function preload(MvcEvent $event) {

		$list_route_need_login = array('User\Controller\Index-profile', 'User\Controller\Index-logout', );
		$controller = $event -> getRouteMatch() -> getParam('controller');
		$action = $event -> getRouteMatch() -> getParam('action');
		$request_route = $controller . '-' . $action;
		$validator = new Validator();
		// if($validator->isValid()){echo "xin chao ".$validator->getUser();}
		if (in_array($request_route, $list_route_need_login)) {
			if (!$validator -> isValid())
				die('ban can phai dang nhap');

		}
		// thay doi ngon ngu. Mac dinh la en_US, bien $userlang dc lay tu thong tin user
		//ngon ngu duoc luu trong thu muc module/Application/language
		//phan nao can translate thi o view dung $this->translate('tu can dich',__NAMESPACE__)
		//set langguege
		$userlang = 'vi_VN';
		$lang = isset($userlang) ? $userlang : 'en_US';
		$translator = $event -> getApplication() -> getServiceManager() -> get('translator');

		$translator -> setLocale($lang) -> setFallbackLocale('en_US');
	}

	public function getAutoloaderConfig() {
		return array(
		//             'Zend\Loader\ClassMapAutoloader' => array(
		//                 __DIR__ . '/autoload_classmap.php',
		//             ),
		'Zend\Loader\StandardAutoloader' => array('namespaces' => array(__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__, ), ), );
	}

	public function getConfig() {
		return
		include __DIR__ . '/config/module.config.php';
	}

	public function getServiceConfig() {
		return array('factories' => array(
			'Category' => function($sm) {
				$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');		
				return $table = new Category(new TableGateway('bee_category', $dbAdapter));
			}
		));
	}

}
