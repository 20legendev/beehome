<?php
namespace Cate\Model;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

use DVGroup\Db\Model\BaseTable;

class TbMusicTitleForead extends BaseTable {
	const IS_ACTIVE = 0;
	const STATUS = 0;
	const NO_STATUS = 1;
	const TYPE_1 = 0;
	//  const TABLE_NAME = 'tb_music_title_foread_tam';// tb_music_title_foread
	const MUSIC_TYPE = 0;
	const VIDEO_TYPE = 1;
	// constant variable, song or playlist
	const SONG_TYPE = 1;
	const PLAYLIST_TYPE = 2;

	public function check() {
		$sql = new Sql($this -> tableGateway -> getAdapter());
		$select = $sql -> select('tb_music_title_foread');
		$select -> where(array('id=2'));
		$statement = $sql -> prepareStatementForSqlObject($select);
		$results = $statement -> execute();
		return $results;
	}

	public function getListMusicWithCateId($cate_id = 0, $type = 0, $sortBy = 'id') {
		if ($cate_id > 0) {
			$select = $this -> tableGateway -> getSql() -> select();
			$select -> join('tb_artist', $this -> tableGateway -> getTable() . '.artist_id=tb_artist.id', array('ar.name_search' => 'name_search'), 'left');
			$select -> where(array('tb_music_title_foread.type' => $type,
			// 'tb_music_title_foread.status'=>self::IS_ACTIVE,
			'tb_music_title_foread.cate_id' => $cate_id, 'tb_music_title_foread.file IS NOT NULL', ));

			$select -> order($sortBy . ' DESC');
			// echo $select->getSqlString();
			$kk = $this -> tableGateway -> selectWith($select);
			$resultSet = new ResultSet();
			$resultSet -> initialize($kk);
			$resultSet -> buffer();
			return $resultSet;
		} else
			return false;

	}

	public function getDetailBySlug($slug, $identify = NULL, $type = 0) {

	}

	/*
	 * Author:
	 * Get music detail by identify
	 * Input: identify (string)
	 * Return: Autocached music information
	 */
	/* Incompleted: Need changes to improve its performance */
	public function getDetailByIdentify($identify = NULL) {
		if (!$identify)
			return false;
		$table_name = $this -> tableGateway -> getTable();
		$select = $this -> tableGateway -> getSql() -> select();
		$select -> join(array('cr'=>'tb_music_title_content_foread'),  
				$table_name. '.id = cr.id_music', 
				array('md_content' => 'content', 'md_description' => 'description'), 'left');
		$select -> join(array('c'=>'tb_composer'),
				$table_name. '.nhacsi_id = c.composer_id', array('composer_name','composer_slug'));
		$select -> join(array('cate'=>'tb_cate'),
				$table_name. '.cate_id = cate.id', array(
						'cate_name'=>'name',
						'cate_name_search'=>'name_search',
						'cate_id'=>'id'
				), 'left');
		$select -> where(array(
			$table_name.'.status' => self::STATUS, 
			$table_name.'.identify' => $identify)
		);
		$result = $this->getCacheableData($select);
		if(count($result) > 0){
			return $result[0];
		}
		return NULL;
	}

	public function updatePageView($id, $view) {
		return $this -> tableGateway -> update(array('view' => $view), array('id' => $id));
	}

	public function getMediaRelated($cate_id, $limit) {
		$select = $this -> tableGateway -> getSql() -> select();
		$select -> where($cate_id);
		//$select->order($order.' DESC');
		$select -> limit($limit);
		//  $select->join('tb_artist',$this->tableGateway->getTable().'.artist_id=tb_artist.id',array('name_search'));
		$select -> order(new \Zend\Db\Sql\Expression('RAND()'));
		//echo $select->getSqlString();
		$kk = $this -> tableGateway -> selectWith($select);
		$resultSet = new ResultSet();
		$resultSet -> initialize($kk);
		$resultSet -> buffer();
		return $resultSet;
	}

	public function getMediabyArtist_id($artist_id, $type = 0, $order = 'id') {
		$artist_id = (int)$artist_id;
		if (!$artist_id)
			return false;
		$select = $this -> tableGateway -> getSql() -> select();
		//$select->join('tb_artist',$this->tableGateway->getTable().'.artist_id=tb_artist.id',array('ar.name_search'=>'name_search'),'left');
		$select -> where(array('tb_music_title_foread.type' => $type, 'tb_music_title_foread.status' => self::IS_ACTIVE, 'tb_music_title_foread.artist_id' => $artist_id, 'tb_music_title_foread.file IS NOT NULL', ));

		$select -> order($order . ' DESC');
		$kk = $this -> tableGateway -> selectWith($select);
		$resultSet = new ResultSet();
		$resultSet -> initialize($kk);
		$resultSet -> buffer();
		return $resultSet;

	}
	
    /*
     * author: KienNT
     * Get music with full information from an artist, include his describe, like...
     * Input: (int) artist_id
     * No require: other
     */
    
	public function getFullByArtistId($artist_id, $type = 0, $limit = 12, $page = 0, $order = 'hitcount', $sort = 'DESC'){
		$artist_id = (int)$artist_id;
		if (!$artist_id)
			return false;
		$table_name = $this->tableGateway->getTable();
		$select = $this -> tableGateway -> getSql() -> select();
		$select->join(array('c'=>'tb_cate'), 'c.id = '.$table_name.'.cate_id', array('cate_name'=>'name', 'cate_search'=>'name_search'));
		$select -> where(array(
			$table_name.'.type' => $type, 
			$table_name.'.status' => self::IS_ACTIVE, 
			'artist_id' => $artist_id, 
			'file IS NOT NULL'
		));

		$select -> order($order . ' ' . $sort);
		$select -> limit($limit);
			$select -> offset($page * $limit);	
		return $this->getCacheableData($select);
	}
	
	/*
	 * author: KienNT
	 * Count all music of an artist
	 * Input: (int) artist_id
	 * No require: other
	 */
	
	public function countByArtistId($artist_id, $type = 0, $limit = 12){
		$artist_id = (int)$artist_id;
		if (!$artist_id)
			return false;
		$table_name = $this->tableGateway->getTable();
		$select = $this -> tableGateway -> getSql() -> select();
		$select -> where(array(
			$table_name.'.type' => $type, 
			$table_name.'.status' => self::IS_ACTIVE, 
			'artist_id' => $artist_id, 
			'file IS NOT NULL'
		));
		return ceil(count($this->getCacheableData($select))/$limit);
	}
	
	public function getRelated($cate_id, $type = 0, $limit = 10) {
		$table_name = $this->tableGateway->getTable();
		$select = $this -> tableGateway -> getSql() -> select();
		$select->join(array('a'=>'tb_artist'), 'a.id = '.$table_name.'.artist_id', array(
			'artist_name'=>'name',
			'artist_name_search'=>'name_search'
		), 'left');
		$select->where(array(
			$table_name.'.cate_id' => $cate_id,
			$table_name.'.type'=>$type
		));
		$select -> limit($limit);
		$select -> order(new \Zend\Db\Sql\Expression('RAND()'));
		
		return $this->getCacheableData($select);
	}

}
