<?php
namespace Cate\Model;

use Zend\Db\Sql\Select;
use DVGroup\Db\Model\BaseTable;

class ArtistCountry extends BaseTable {
		
	public $id;
	public $name;
	public $created_time;
	
	public function addNew($array){
		return $this->tableGateway->insert($array);
	}
	
	public function getAll(){
		$select = $this->tableGateway->getSql()->select();
		$select->order('country_order ASC');
		$result = $this->tableGateway->selectWith($select);
		$result->buffer();
		
		$return_array = array();
		foreach($result as $obj){
			$return_array[] = $obj;
		} 
		return $return_array;
	}
	
	public function getBySlug($slug){
		$result = $this->tableGateway->select(array(
			'country_en'=>$slug
		));
		$row = $result->current();
		return $row ? $row : NULL;
	}

}
