<?php
namespace Category\Model;
use Pi\Db\Model\BaseTable;

class Category extends BaseTable{

	public function getHomeMenu() {
		$select = $this->tableGateway->getSql()->select();
		$select->where('parent_id=0');
		$select -> order('order ASC');
		return $this -> tableGateway -> selectWith($select);
	}
}
