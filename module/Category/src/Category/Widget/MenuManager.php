<?php
namespace Category\Widget;
use Pi\Helper\BaseWidget;
use Zend\View\Model\ViewModel;

class MenuManager extends BaseWidget{

    public function topMenu(){
        $view = new ViewModel();
        $catObj = $this->getTable('Category');
        $list = $catObj->getHomeMenu();
        $view = $this->getView()->render('category/widget/top-menu', [
          'list' => $list
        ]);
        return $view;
    }
}
?>