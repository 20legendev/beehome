<?php

namespace Cate\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use DVGroup\Redis\Redis;
use Cate\Model\Cate;
use Cate\Model\CateTable;
use Cate\Model\TbMusicTitleForead;
use Zend\Paginator\Paginator;
use DVGroup\Operation\BaseController;

class MusicController extends BaseController {
	
	public function musicAction() {
		$view = new ViewModel ();
		$params = $this->params ();
		$slug = $params->fromRoute ( 'slug', 0 );
		$identify = $params->fromRoute ( 'identify');
		
		
		$tb_music = $this->getTable ( 'Cate\Model\TbMusicTitleForead' );
		$music_detail = $tb_music->getDetailByIdentify($identify);
		if(!$music_detail){
			return $this->layout ( 'error/404' );
		}
		$subject = $this->getTable('SubjectMusicTable');
		$subject_detail = $subject->getSubjectByMusicId($music_detail['id']);
		
		$music_content_obj = $this->getTable('MusicContentForead');
		$music_content = $music_content_obj->getByMusicId(intval($music_detail['id']));
		
		$artist_profile = $this->forward ()->dispatch ( 'Artist\Controller\Widget', array (
				'action' => 'artist-music',
				'artist_id' => $music_detail['artist_id']
		) );
		$music_related = $this->forward ()->dispatch ( 'Artist\Controller\Widget', array (
				'action' => 'media-related',
				'artist_id' => $music_detail['artist_id']
		) );
		
		
		$view->addChild($music_related, 'music_related');
		$view->addChild($artist_profile, 'artist_profile');	
		$view->music_content = $music_content;
		$view->music_detail = $music_detail;
		$view->subject_detail = $subject_detail;
		
		return $view;
		
		
		
		
		
		
		
		
		
		
		$cate = $this->getTable ( 'Cate\Model\TbCate' );
		
		$allCate = $cate -> globAllCate ();
		
		$redisKey = 'tb_music_title_foread_rd:music:identify:a:' . $identify;
		// kiem tra redis
		// $redis->setOption(\Redis::OPT_SERIALIZER, \Redis::SERIALIZER_NONE);
		$redis = new Redis ();
		$itemDetail = $redis->hgetall ( $redisKey );
		
		if (! $itemDetail || empty ( $itemDetail )) {
			
			$itemDetail = $this->getTable ( 'Cate\Model\TbMusicTitleForead' )->getDetailByIdentify ( $identify );
			if (! $itemDetail) {
				return $this->layout ( 'error/404' );
			}
			// luu vao redis
			$itemDetail = get_object_vars ( $itemDetail );
			$redis->hmset ( $redisKey, $itemDetail );
			// echo 'db';
		}
		// kiem tra slug
		if ($itemDetail ['slug'] != $slug) {
			return $this->redirect ()->toRoute ( 'Music', array (
					'action' => 'music',
					'slug' => $itemDetail ['slug'],
					'identify' => $identify 
			) );
		}
		// kiem tra type
		if ($itemDetail ['type'] == 1) {
			return $this->redirect ()->toRoute ( 'Video', array (
					'action' => 'video',
					'slug' => $itemDetail ['slug'],
					'identify' => $identify 
			) );
		}
		// tang visitor cho trang
		$this->getTable ( 'Cate\Model\TbMusicTitleForead' )->updatePageView ( $itemDetail ['id'], $itemDetail ['view'] + 1 );
		$redis->hIncrBy ( $redisKey, 'view', 1 );
		
		$view->itemDetail = $itemDetail;
		$menu = $this->forward ()->dispatch ( 'Cate\Controller\Widget', array (
				'action' => 'menu' 
		) );
		$view->addChild ( $menu, 'menu' );
		
		// so luong item cho related
		$item = 10;
		// chon cache tuong ung voi related tren redis
		$rand = (( int ) $itemDetail ['id']) % 5;
		// tim cate video
		$v_cate_id = \DVGroup\Common\CommonLibs::random_cate ( $allCate, $itemDetail ['cate_id'] );
		
		$musicrelated = $this->forward ()->dispatch ( 'Cate\Controller\Widget', array (
				'action' => 'musicrelated',
				'cate_id' => $itemDetail ['cate_id'],
				'rand' => $rand,
				'limit' => $item 
		) );
		$view->addChild ( $musicrelated, 'musicrelated' );
		$videorelated = $this->forward ()->dispatch ( 'Cate\Controller\Widget', array (
				'action' => 'videorelated',
				'cate_id' => $v_cate_id,
				'rand' => $rand,
				'limit' => $item 
		) );
		$view->addChild ( $videorelated, 'videorelated' );
		$albumrelated = $this->forward ()->dispatch ( 'Cate\Controller\Widget', array (
				'action' => 'albumrelated',
				'cate_id' => $itemDetail ['cate_id'],
				'rand' => $rand,
				'limit' => $item 
		) );
		$view->addChild ( $albumrelated, 'albumrelated' );
		$music_artist_related = $this->forward ()->dispatch ( 'Cate\Controller\Widget', array (
				'action' => 'musicartistrelated',
				'artist_id' => $itemDetail ['artist_id'],
				'rand' => $rand,
				'limit' => $item 
		) );
		$view->addChild ( $music_artist_related, 'music_artist_related' );
		$video_artist_related = $this->forward ()->dispatch ( 'Cate\Controller\Widget', array (
				'action' => 'videoartistrelated',
				'artist_id' => $itemDetail ['artist_id'],
				'rand' => $rand,
				'limit' => $item 
		) );
		$view->addChild ( $video_artist_related, 'video_artist_related' );
		
		// var_dump($allCate);
		return $view;
	}
	
	// Video------------------------------------------------------------------------
	public function videoAction() {
		$view = new ViewModel ();
		$allCate = $this->getTable ( 'Cate\Model\TbCate' )->globAllCate ();
		$slug = $this->params ()->fromRoute ( 'slug', 0 );
		$identify = $this->params ()->fromRoute ( 'identify', 0 );
		$redisKey = 'tb_music_title_foread_rd:video:identify:' . $identify;
		// kiem tra redis
		// $redis->setOption(\Redis::OPT_SERIALIZER, \Redis::SERIALIZER_NONE);
		$redis = new Redis ();
		$itemDetail = $redis->hgetall ( $redisKey );
		
		if (! $itemDetail || empty ( $itemDetail )) {
			
			$itemDetail = $this->getTable ( 'Cate\Model\TbMusicTitleForead' )->getDetailByIdentify ( $identify );
			if (! $itemDetail) {
				return $this->layout ( 'error/404' );
			}
			// luu vao redis
			$itemDetail = get_object_vars ( $itemDetail );
			$redis->hmset ( $redisKey, $itemDetail );
			// echo 'db';
		}
		// kiem tra slug
		if ($itemDetail ['slug'] != $slug) {
			return $this->redirect ()->toRoute ( 'Video', array (
					'action' => 'video',
					'slug' => $itemDetail ['slug'],
					'identify' => $identify 
			) );
		}
		// kiem tra type
		if ($itemDetail ['type'] == 0) {
			return $this->redirect ()->toRoute ( 'Music', array (
					'action' => 'detail',
					'slug' => $itemDetail ['slug'],
					'identify' => $identify 
			) );
		}
		// tang visitor cho trang
		$this->getTable ( 'Cate\Model\TbMusicTitleForead' )->updatePageView ( $itemDetail ['id'], $itemDetail ['view'] + 1 );
		$redis->hIncrBy ( $redisKey, 'view', 1 );
		
		$menu = $this->forward ()->dispatch ( 'Cate\Controller\Widget', array (
				'action' => 'menu' 
		) );
		$view->addChild ( $menu, 'menu' );
		
		$view->itemDetail = $itemDetail;
		// so luong item cho related
		$item = 10;
		// chon cache tuong ung voi related tren redis
		$rand = (( int ) $itemDetail ['id']) % 5;
		// tim cate music va album
		$m_cate_id = \DVGroup\Common\CommonLibs::random_m_cate ( $allCate, $itemDetail ['cate_id'] );
		
		$musicrelated = $this->forward ()->dispatch ( 'Cate\Controller\Widget', array (
				'action' => 'musicrelated',
				'cate_id' => $m_cate_id,
				'rand' => $rand,
				'limit' => $item 
		) );
		$view->addChild ( $musicrelated, 'musicrelated' );
		$videorelated = $this->forward ()->dispatch ( 'Cate\Controller\Widget', array (
				'action' => 'videorelated',
				'cate_id' => $itemDetail ['cate_id'],
				'rand' => $rand,
				'limit' => $item 
		) );
		$view->addChild ( $videorelated, 'videorelated' );
		$albumrelated = $this->forward ()->dispatch ( 'Cate\Controller\Widget', array (
				'action' => 'albumrelated',
				'cate_id' => $m_cate_id,
				'rand' => $rand,
				'limit' => $item 
		) );
		$view->addChild ( $albumrelated, 'albumrelated' );
		$music_artist_related = $this->forward ()->dispatch ( 'Cate\Controller\Widget', array (
				'action' => 'musicartistrelated',
				'artist_id' => $itemDetail ['artist_id'],
				'rand' => $rand,
				'limit' => $item 
		) );
		$view->addChild ( $music_artist_related, 'music_artist_related' );
		$video_artist_related = $this->forward ()->dispatch ( 'Cate\Controller\Widget', array (
				'action' => 'videoartistrelated',
				'artist_id' => $itemDetail ['artist_id'],
				'rand' => $rand,
				'limit' => $item 
		) );
		$view->addChild ( $video_artist_related, 'video_artist_related' );
		
		// var_dump($allCate);
		return $view;
	}
}
?>