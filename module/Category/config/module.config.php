<?php
return array(
  'controllers'     =>[
            'invokables'     =>[   
                    

                    ]],
    'router'          =>[
            'routes'         => [
              'ProductCategory' => [
                'type'    => 'segment',
                             'options' => [ 'route'         => '/shop/[:slug][.:category_id].html', 
                                            'constraints'   =>['slug'   => '[a-zA-Z0-9_-]*'
                                                               ],                                         
                                            'defaults'      => ['controller' => 'Category\Controller\Index',
                                                                'action'     => 'show']]],

    
            ],
        ],
     'view_manager' => [
         'template_path_stack' => [
             'cate' => __DIR__ . '/../view',
         ],
     ],
 );
 ?>