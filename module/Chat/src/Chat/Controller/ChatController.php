<?php
    namespace Chat\Controller;

    use Zend\Mvc\Controller\AbstractActionController; 
    use Chat\Model\TbChat;

    class ChatController extends AbstractActionController
    {
        // Test một số chức năng của RedisActiveRecord
        public function testAction()
        {   
            // Delete toàn bộ database
            TbChat::deleteAll(); //Có thể gọi deleteAll($condition) để xóa các row mình muốn, $condition tham khảo các phần tiếp theo
            //die();
            // Tạo row mới

            //cách 1
            $model = new TbChat();
            $model->user_from = 3;
            $model->user_to = 5;
            $model->room_id = 10;
            $model->content = 'hello Nam';
            $model->save(); //Hoặc $model->insert();
            //die;
            //cách 2
            $model = new TbChat();
            $model['user_from'] = 5;
            $model['user_to'] = 3;
            $model['room_id'] = 10;
            $model['content'] = 'hello Tho'; 
            $model->save(); //Hoặc $model->insert();
            //die;
            //cách 3
            $model = new TbChat();
            $model->load(['user_from'=>3,'user_to'=>5,'room_id'=>10,'content'=>'How are you']);
            $model->save(); //Hoặc $model->insert();
            //die;
            $model = new TbChat();
            $model->load(['user_from'=>5,'user_to'=>3,'room_id'=>10,'content'=>'I\'m OK, thanks']);
            $model->save();

            $model = new TbChat();
            $model->load(['user_from'=>7,'user_to'=>3,'room_id'=>11,'content'=>'Trưa đi đâu thế']);
            $model->save();

            $model = new TbChat();
            $model->load(['user_from'=>3,'user_to'=>7,'room_id'=>11,'content'=>'Tao đi ăn']);
            $model->save();

            $model = new TbChat();
            $model->load(['user_from'=>7,'user_to'=>3,'room_id'=>11,'content'=>'Thế mày ko đi đến nhà thằng Quang à']);
            $model->save();

            $model = new TbChat();
            $model->load(['user_from'=>3,'user_to'=>7,'room_id'=>11,'content'=>'Không, nó gọi bồ về ăn cùng rồi']);
            $model->save();

            $model = new TbChat();
            $model->load(['user_from'=>3,'user_to'=>7,'room_id'=>11,'content'=>'Thôi, tao vào làm việc đây, Bye bye mày.']);
            $model->save();

            $model = new TbChat();
            $model->load(['user_from'=>7,'user_to'=>3,'room_id'=>11,'content'=>'Okie, bb']);
            $model->save();

            //Test validate
            $model = new TbChat(); 
            $model->user_to = 5;
            $model->room_id = 'xxx'; 
            $model->date = 'zxt';
            if (!$model->save()) {
                echo '<pre>';
                print_r($model->errors);
                echo '</pre>';
                die('|');
            }

            // hoặc            
            if ($model->hasErrors()) {
                echo '<pre>';
                print_r($model->errors);
                echo '</pre>';
                die('|');
            }

            //Lấy dữ liệu ra

            //Tìm kiếm theo khóa chính            
            $model = TbChat::findOne(2);
            if ($model) {
                echo  $model->content; 
                echo  $model['content'];
                echo '<pre>';
                print_r($model);  
                echo '</pre>';
                die('|'); 
            }  else echo 'not found';

            //Tìm tất cả theo điều kiện và test update
            $models = TbChat::findAll(['room_id'=>11,['>=','date','2014-10-20 10:21:20']]);  //có thể dùng với findAll() để tìm tất cả database
            if ($models) {  
                foreach ($models as $model) {
                    $model->content = $model->content.'- edited';
                    $model->update(); //hoặc $model->save();
                }    
            }  else echo 'not found';   

            //Tìm theo ORM

            //Trả về object
            $findModels = TbChat::find()->where(['room_id'=>10,'content'=>'hello Nam'])->orWhere(['id'=>4])->limit(20)->orderBy(['content'=>SORT_DESC])->all(); // dùng ->one() để lấy 1 giá trị đầu tiên tìm đc
            echo '<pre>';
            print_r($findModels);
            echo '</pre>';
            die('|');

            //Trả về Array, chỉ cần thêm ->asArray() vào bất kì chỗ nào trước ->all() hoặc ->one()

            $findModels = TbChat::find()->where(['room_id'=>10,'content'=>'hello Nam'])->orWhere(['id'=>4])->limit(20)->orderBy(['content'=>SORT_DESC])->asArray()->all(); // dùng ->one() để lấy 1 giá trị đầu tiên tìm đc
            echo '<pre>';
            print_r($findModels);
            echo '</pre>';
            die('|'); 

            // Để cache kết quả querry ta dùng ->cache(), Mỗi câu querry sẽ sinh ra 1 key lưu lại kết quả trong thư mục :cache, để lần sau gặp tự lấy ra chứ ko cần thực hiện,
            // mỗi khi có bất kì sự thay đổi nào trong database (các sự kiện insert, save, update, updateAll, delete, deleteAll) thì các key này tự động bị xóa để build lại     
            $findModels = TbChat::find()->where(['room_id'=>10])->cached()->all();

            //Một số querry phức tạp hơn
            $findModels = TbChat::find()->select(['user_from','content'])->where([['>=','room_id',5],['>','id',5],['like','content','%h%']])->orWhere([['like','content','%bye%']])->limit(20)->orderBy(['content'=>SORT_DESC])->asArray()->cached()->all();  
            echo '<pre>';
            print_r($findModels);
            echo '</pre>';
            die('|');
            // select distinct
            $findModels = TbChat::find()->select([['user_from','user_to'],'distinct'])->asArray()->cached()->all();
            echo '<pre>';
            print_r($findModels);
            echo '</pre>';
            die('|');

            $findModels = TbChat::find()->orderBy(['room_id'=>SORT_DESC])->asArray()->cached()->all();
            echo '<pre>';
            print_r($findModels);
            echo '</pre>';
            die('|');

            $findModels = TbChat::find()->select([['room_id'],'average'])->cached()->all(); //Lấy trung bình cộng
            echo '<pre>';
            print_r($findModels);
            echo '</pre>';
            die('|');
            // lưu ý, các hàm lấy trung bình cộng và lấy tổng thì phải dùng phương thức ->all();
            $findModels = TbChat::find()->select([['room_id'],'sum'])->cached()->all(); //Lấy tổng
            echo '<pre>';
            print_r($findModels);
            echo '</pre>';
            die('|'); 

            // Thực hiện các lệnh của Redis: Tất cả các lệnh của Redis đều có thể thực hiện đc luôn và gọi trực tiếp từ model
            //Eg:
            $redis = new TbChat(); // Hoặc $redis = new RedisActive();
            $redis->set('h','abc');
            $redis->get('h');
            $redis->del('h');
            $redis->hSet('h', 'a', 'x');
            $redis->hSet('h', 'b', 'y');
            $redis->hSet('h', 'c', 'z');
            $redis->hSet('h', 'd', 't'); 
            // .....
            //Tham khảo: https://github.com/nicolasff/phpredis
        }
    }
