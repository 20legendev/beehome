<?php

namespace News\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use DVGroup\Db\Model\BaseTable;

class ArticleTag extends BaseTable{
	
	public function getNewsByTag($tag, $limit = 0, $page = 0) {
		$tag = str_replace('-', ' ', $tag);
		$table_name = $this->tableGateway->getTable();
		
		$select = $this->tableGateway->getSql()->select();
		$select->columns(array('tag_name'));
		$select->join(array('tl'=>'tb_article_tag_relation'), $table_name.'.id = tl.tag_id');
		$select->join(array('a'=>'tb_article_info_new'), 'a.id = tl.article_id');
		$select->where(array(
				$table_name . '.tag_name' => $tag
		));
		$select->order('a.created_time DESC');
		if($limit != 0){
			$select->limit($limit);
			$select->offset($page * $limit);
		}
		return $this->getCacheableData($select);
	}
	
	public function countByTag($tag){
		$data = $this->getNewsByTag($tag);
		return count($data);
	}
}