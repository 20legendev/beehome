<?php
namespace News\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use DVGroup\Redis\Redis;
 class IndexController extends AbstractActionController
 {
    protected function getTbCategoryArticle()
    {
        return $this->getServiceLocator()->get('TbCategoryArticle');        
    }
    protected function getCrawle()
    {
        return $this->getServiceLocator()->get('Crawle');        
    }
    protected function getTbArticle()
    {
        return $this->getServiceLocator()->get('TbArticle');        
    }
    public function indexAction()
    {
        $view=new ViewModel();
        $allcategory=$this->getTbCategoryArticle()->All_Article_Cate_array();
        $view->allcate=$allcategory;
        //$this->getCrawle()->intesec();
        
        
        return $view;

    }
    public function categoryAction()
    {
        $view=new ViewModel();
        $slug=$this->params()->fromRoute('slug',0);
        $page=$this->params()->fromRoute('page',0);
        if(!$page)$page=1;
        $limit=15;
        $keyrd='_CACHE:NEWS:Category:'.$slug.':limit:'.$limit.':page:'.$page;
        $redis=new Redis();
        $list_item_arr=$redis->_Get($keyrd);
        if(!$list_item_arr||empty($list_item_arr))
        {
            $ListItem=$this->getTbArticle()->getNews_By_Category_slug($slug,$page,$limit);
            $list_item_arr=[];
            foreach ($ListItem as $value)
            {
                if($value)$list_item_arr[]=get_object_vars($value);
            }
            if(!empty($list_item_arr))$redis->_Set($keyrd,$list_item_arr,7200);           
        }
        //var_dump($list_item_arr)       ; 
        $cat=$this->getTbCategoryArticle()->getCateBySllug($slug);        
        $view->ListItem=$list_item_arr;
        $view->cat=$cat;
        $view->page=$page;
        return $view;
    }
    public function detailAction()
    {
        $view=new ViewModel();
       // $kk=new Neo4j();
        $slug=$this->params()->fromRoute('slug',0);
        $identify=$this->params()->fromRoute('identify',0);
        
        $keyrd='NEWS:tb_article_detail:'.$identify;
        $redis=new Redis();
        $item_arr=$redis->hgetall($keyrd);
        if(!$item_arr||empty($item_arr))
        {
            $item=$this->getTbArticle()->getNewsDetailBySlug($slug);
            if($item){$item_arr=get_object_vars($item);$redis->hmset($keyrd,$item_arr);}
        }
        $view->item=$item_arr;
        
        //$rdt=$this->getServiceLocator()->get('TbArticleRd');
        //$rdt->mysql_to_redis('Test_folder','music_5000_list');
        
       return  $view;
    }
    
 }
?>