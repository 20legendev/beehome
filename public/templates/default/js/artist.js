$( document ).ready(function() {
	$( "#btn-more-info" ).toggle(function() {
		$('#artist-info').removeClass("rows4");
        $(this).html('Thu gọn').parent().children('span').removeClass("pg-dow").addClass("pg-top");
	}, function() {
		$('#artist-info').addClass("rows4");
        $(this).html('Xem thêm').parent().children('span').removeClass("pg-top").addClass("pg-dow");
	});
    $(function() {
    // Acquire a reference to the carousel
    var carousel = $('.sky-carousel').carousel({
        itemWidth: 260,
        itemHeight: 260,
        distance: 12,
        selectedItemDistance: 75,
        selectedItemZoomFactor: 1,
        unselectedItemZoomFactor: 0.5,
        unselectedItemAlpha: 0.6,
        motionStartDistance: 210,
        topMargin: 10,
        gradientStartPoint: 0.35,
        gradientOverlayColor: "#ebebeb",
        gradientOverlaySize: 190,
        enableKeyboard: true, // dùng bàn phím để đổi ảnh
        autoSlideshow: true, // tự động thay đổi ảnh
        autoSlideshowDelay: 4.5, // thời gian thay đổi ảnh
        selectByClick: true // click vào chuyển ảnh
    });
    // Select the 2nd item with 4 second selection animation
    carousel.select(1, 4);
    // Select the next item with no animation
    carousel.selectNext(0);
    // Select the previous item with 2 second selection animation
    carousel.selectPrev(2);
    // Start auto slideshow
    carousel.startAutoSlideshow();
    // Stop auto slideshow
    carousel.stopAutoSlideshow();
    // Print the selected item's index to the console
    carousel.on('itemSelected.sc', function(evt) {
        console.log(evt.item.index());
    });
    // Print the closest item's index to the console
    carousel.on('closestItemChanged.sc', function(evt) {
        console.log(evt.item.index());
    });
    // Append some text to the body when the selection animation has started
    carousel.on('selectionAnimationStart.sc', function(evt) {
        $('body').append('<p>Animation has started...</p>');
    });
    // Append some text to the body when the selection animation has ended
    carousel.on('selectionAnimationEnd.sc', function(evt) {
        $('body').append('<p>Animation has ended...</p>');
    });
    });
    LazyLoad.init();
});
