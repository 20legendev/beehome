
$(document).ready(function () {
    $(".subject-slide").jCarouselLite({
        vertical: false,
        visible: 4,
        auto: 0,
        speed: 1500,
        btnNext: ".btnnext",
        btnPrev: ".btnprev"
    });

    $(".subject-slide-related").jCarouselLite({
        vertical: false,
        visible: 3,
        auto: 0,
        speed: 1500,
        btnNext: ".btnnext",
        btnPrev: ".btnprev"
    });

    $("#all input").focus();

    var hdbGender = $('#divGender').AdvanceHiddenDropbox({
        id: 'divGenderOptions',
        hddValue: 'hdGender'
    });

    var hdbGender = $('#divCountry').AdvanceHiddenDropbox({
        id: 'divCountryOptions',
        hddValue: 'hdCountry'
    });

    var hdbGender = $('#divMelody').AdvanceHiddenDropbox({
        id: 'divMelodyOptions',
        hddValue: 'hdMelody'
    });

    var hdbGender = $('#divOther').AdvanceHiddenDropbox({
        id: 'divOtherOptions',
        hddValue: 'hdOther'
    });

    var hdbPoint1 = $('#divPoint1').AdvanceHiddenDropbox({
        id: 'divPointOptions1',
        hddValue: 'hdPoint1'
    });
    var hdbPoint2 = $('#divPoint2').AdvanceHiddenDropbox({
        id: 'divPointOptions2',
        hddValue: 'hdPoint2'
    });

    var hdbPointSelf1 = $('#divPointSelf1').AdvanceHiddenDropbox({
        id: 'divPointSelfOptions1',
        hddValue: 'hdPointSelf1'
    });

    var hdbPointSelf2 = $('#divPointSelf2').AdvanceHiddenDropbox({
        id: 'divPointSelfOptions2',
        hddValue: 'hdPointSelf2'
    });
});

(function ($) {
    $(function () {
        $('.tabs-search li a').click(function (e) {
            e.preventDefault();
            $('.tabs-search li').removeClass('active');
            $(this).closest('li').addClass('active');
            $('.tab-pane').css('display','none');
            var strOpenTab = $(this).attr('href').split('#')[1];
            $('#' + strOpenTab).css('display','block');
        });
    });

    
})(jQuery);
 


 $(function () {        
        ___isIE = /MSIE (\d+\.\d+);/.test(navigator.userAgent);
        if (___isIE) {
            $('.placehold').each(function(){
                 var defaultText = $(this).attr('placeholder');
                $('.placehold').css('color', '#777');              

                if ($(this).val().length == 0) {
                    $(this).val(defaultText);
                }

                $(this).blur(function () {
                    if ($(this).val().length == 0) {
                        $(this).val(defaultText);
                    }
                });

                $(this).focus(function () {

                    if ($(this).val() == defaultText)
                        $(this).val('');
                });
            }); 
        }
});

