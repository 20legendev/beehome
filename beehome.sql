-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 23, 2015 at 11:47 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `beehome`
--

-- --------------------------------------------------------

--
-- Table structure for table `bee_category`
--

CREATE TABLE IF NOT EXISTS `bee_category` (
`category_id` int(6) unsigned NOT NULL,
  `category_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_slug` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(6) DEFAULT '0',
  `is_active` int(6) DEFAULT '1',
  `order` int(6) DEFAULT '0',
  `bee_categorycol` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bee_category`
--

INSERT INTO `bee_category` (`category_id`, `category_name`, `category_slug`, `parent_id`, `is_active`, `order`, `bee_categorycol`) VALUES
(1, 'Phòng khách', 'noi-that-phong-khach', 0, 1, 1, NULL),
(2, 'Phòng ngủ', 'noi-that-phong-ngu', 0, 1, 2, NULL),
(3, 'Phòng ăn - bếp', 'noi-that-phong-an-bep', 0, 1, 3, NULL),
(4, 'Phòng làm việc', 'noi-that-phong-lam-viec', 0, 1, 4, NULL),
(5, 'Phòng trẻ em', 'noi-that-phong-tre-em', 0, 1, 5, NULL),
(6, 'Đồ trang trí', 'do-trang-tri', 0, 1, 6, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bee_product`
--

CREATE TABLE IF NOT EXISTS `bee_product` (
`product_id` int(6) NOT NULL,
  `product_name` text COLLATE utf8_unicode_ci,
  `product_price` int(11) DEFAULT NULL,
  `sale_price` int(11) DEFAULT NULL,
  `quantity` int(6) DEFAULT NULL,
  `product_desc` text COLLATE utf8_unicode_ci,
  `featured_image_id` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bee_product_category`
--

CREATE TABLE IF NOT EXISTS `bee_product_category` (
`product_category_id` int(6) NOT NULL,
  `product_id` int(6) DEFAULT NULL,
  `category_id` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bee_product_image`
--

CREATE TABLE IF NOT EXISTS `bee_product_image` (
`product_image_id` int(6) NOT NULL,
  `product_id` int(6) DEFAULT NULL,
  `image_link` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bee_category`
--
ALTER TABLE `bee_category`
 ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `bee_product`
--
ALTER TABLE `bee_product`
 ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `bee_product_category`
--
ALTER TABLE `bee_product_category`
 ADD PRIMARY KEY (`product_category_id`);

--
-- Indexes for table `bee_product_image`
--
ALTER TABLE `bee_product_image`
 ADD PRIMARY KEY (`product_image_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bee_category`
--
ALTER TABLE `bee_category`
MODIFY `category_id` int(6) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `bee_product`
--
ALTER TABLE `bee_product`
MODIFY `product_id` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bee_product_category`
--
ALTER TABLE `bee_product_category`
MODIFY `product_category_id` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bee_product_image`
--
ALTER TABLE `bee_product_image`
MODIFY `product_image_id` int(6) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
